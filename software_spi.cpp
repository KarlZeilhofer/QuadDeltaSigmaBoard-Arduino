
#include "software_spi.h"


#define __AVR_ATmega328__


#if  (defined(__AVR_ATmega2560__) || defined(__AVR_ATmega328__))
  //for example 8cycles as one SPI period, for Arduino mega2560 16MHz, SPI 2MHz,  tested passed for AD7730!
  //for DUE 84MHz,  need longer nop cycles to keep waveform!  -> multiply each delay by 6 
  #define DUR_CYCLES_CLK_active  4   // duration of active clock
  #define DUR_CYCLES_CLK_idle  4  // duration of idle clock
#else
  #error "checking your board and MCU main frequency and set proper delay for software_SPI bit-banging"
#endif


static inline void delayCycles(uint16_t cycles)
{
    for (volatile uint16_t i=0; i<cycles; i++)
        __asm__("nop\n\t");
       // __nop();
}

uint8_t SoftwareSPI::bits[8]={0};
uint8_t SoftwareSPI::pinActiveCS=0;

 
SoftwareSPI::SoftwareSPI(uint8_t SCK, uint8_t MISO, uint8_t MOSI, uint8_t CS, 
  bool clockPolarity, bool clockPhase, bool msbFirst)
{
  pinSCK = SCK;
  pinMISO = MISO;
  pinMOSI = MOSI;
  pinCS = CS;
  this->clockPolarity = clockPolarity;
  this->clockPhase = clockPhase;
  this->msbFirst = msbFirst;

  pinMode(pinCS, OUTPUT);
  pinMode(pinMISO, INPUT);
  pinMode(pinMOSI, OUTPUT);
  pinMode(pinSCK, OUTPUT);
  //
  unselect();
  if(clockPolarity)
    digitalWrite(pinSCK, HIGH);
  else
    digitalWrite(pinSCK, LOW);

  if(msbFirst) 
    for(uint8_t i=0; i<8; i++)
      msk[i] = (uint8_t)1 << (7-i); // msk[] = {0x80, 0x40, 0x20, 0x10, 0x8, 0x4, 0x2, 0x1};
  else 
    for(uint8_t i=0; i<8; i++)
      msk[i] = 1 << i; // msk[] = {0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80};
}

// reading MISO allways before the typical "capture clock edge"
// auto chip-select, but do a manual unselect, if needed!
uint8_t SoftwareSPI::transfer(uint8_t b) 
{
  uint8_t reply=0;
  
  if(digitalRead(pinCS))
  {
    select(); // auto-select

    // when clockPhase==true, we do an initial idle-delay, 
    // so the chip can power up the interface. 
    // for clockPhase == false, we do an idle-delay anyway. 
    if(clockPhase)
    {
      delayCycles(DUR_CYCLES_CLK_idle);
    }
  }


  
  for(uint8_t _bit = 0;_bit < 8;_bit++)
  {
    if(clockPhase)
    {
      if(clockPolarity)
        digitalWrite(pinSCK, LOW);  
      else
        digitalWrite(pinSCK, HIGH); 
      digitalWrite(pinMOSI, !!(b & msk[_bit]));

      delayCycles(DUR_CYCLES_CLK_active); // slave is setting up MISO now. 

      bits[_bit] = digitalRead(pinMISO); 
      if(clockPolarity)
        digitalWrite(pinSCK, HIGH); 
      else
        digitalWrite(pinSCK, LOW);  
     
      delayCycles(DUR_CYCLES_CLK_idle); 
    }
    else
    {
      digitalWrite(pinMOSI, !!(b & msk[_bit]));
      delayCycles(DUR_CYCLES_CLK_idle); // slave is setting up MISO now. 

      bits[_bit] = digitalRead(pinMISO); // read MISO

      if(clockPolarity)
        digitalWrite(pinSCK, LOW); 
      else
        digitalWrite(pinSCK, HIGH);
      
      delayCycles(DUR_CYCLES_CLK_active); 
       
      if(clockPolarity)
        digitalWrite(pinSCK, HIGH);
      else
        digitalWrite(pinSCK, LOW);  // data will change at falling edge
    }
  }

  reply = bitsToByte();

  return reply;
}

uint8_t SoftwareSPI::transfer(uint8_t* data, uint8_t count)
{
  for(uint8_t i=0; i<count; i++)
  {
    data[i] = transfer(data[i]);
  }
  return count;
}


uint8_t SoftwareSPI::bitsToByte()
{ 
  if(msbFirst)
  {
    return (bits[0] << 7 | bits[1] << 6 | bits[2] << 5 | bits[3] << 4 | 
            bits[4] << 3 | bits[5] << 2 | bits[6] << 1 | bits[7] << 0);
  }
  else
  {
    return (bits[0] << 0 | bits[1] << 1 | bits[2] << 2 | bits[3] << 3 | 
            bits[4] << 4 | bits[5] << 5 | bits[6] << 6 | bits[7] << 7);
  }
}

