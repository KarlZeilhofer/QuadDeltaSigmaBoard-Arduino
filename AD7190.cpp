/***************************************************************************//**
 *   @file   AD7190.c
 *   @brief  Implementation of AD7190 Driver.
 *   @author DNechita (Dan.Nechita@analog.com)
********************************************************************************
 * Copyright 2012(c) Analog Devices, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *  - The use of this software may or may not infringe the patent rights
 *    of one or more patent holders.  This license does not release you
 *    from the requirement that you obtain separate licenses from these
 *    patent holders to use this software.
 *  - Use of the software either in source or binary form, must be run
 *    on or directly connected to an Analog Devices Inc. component.
 *
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
********************************************************************************
 *   SVN Revision: 903
*******************************************************************************/

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "AD7190.h"     // AD7190 definitions.
#include <stdint.h>
#include "uartdebug.h"
#include <Arduino.h> 

uint8_t sspiHeap[4*sizeof(SoftwareSPI)];
uint8_t sspiHeapIndex = 0;



/***************************************************************************//**
 * @brief Checks if the AD7190 part is present.
 *
 * @return status - Indicates if the part is present or not.
*******************************************************************************/
AD7190::AD7190(uint8_t pinCS)
{
    
PRINT_LINE;

    // new-operator with specific memory address
    SoftwareSPI* address = (SoftwareSPI*) 
        (sspiHeap + (sspiHeapIndex++) * sizeof(SoftwareSPI));
    sspi = new(address) SoftwareSPI(pinSCK, pinMISO, pinMOSI, pinCS, true, true, true);
}

uint8_t AD7190::Init()
{
    uint8_t regVal = 0;
PRINT_LINE;
    Reset();

PRINT_LINE;
    /* Allow at least 500 us before accessing any of the on-chip registers. */
    delay(1);

PRINT_LINE;

    Select();
    regVal = GetRegisterValue(REG_ID, 1, 0);
    Unselect();

PRINT_LINE;

    if( (regVal & ID_MASK) != ID_AD7190)
    {
        sprintf(__print_line_buf, "regVal = %d\n", regVal);
        print(__print_line_buf);
PRINT_LINE;
        return  0;
    }

PRINT_LINE;

    return 1;
}

/***************************************************************************//**
 * @brief Writes data into a register.
 *
 * @param registerAddress - Address of the register.
 * @param registerValue - Data value to write.
 * @param bytesNumber - Number of bytes to be written.
 * @param modifyCS - Allows Chip Select to be modified.
 *
 * @return none.
 *
 * @changed by Karl Zeilhofer to be endianness independent!
*******************************************************************************/
void AD7190::SetRegisterValue(uint8_t registerAddress,
                             uint32_t registerValue,
                             uint8_t bytesNumber,
                             uint8_t modifyCS)
{
    uint8_t writeCommand[5] = {0, 0, 0, 0, 0};
    uint8_t bytesNr         = bytesNumber;
    
    writeCommand[0] = COMM_WRITE |
                      COMM_ADDR(registerAddress);

    int shift=0;
    while(bytesNr>0)
    {
    	writeCommand[bytesNr] = (registerValue >> shift) & 0xff; // LSByte is last in writeCommand.
    	shift += 8;
    	bytesNr--;
    }
    sspi->transfer(writeCommand, bytesNumber + 1);
    modifyCS++;// remove not-used warning
}

/***************************************************************************//**
 * @brief Reads the value of a register.
 *
 * @param registerAddress - Address of the register.
 * @param bytesNumber - Number of bytes that will be read.
 * @param modifyCS    - Allows Chip Select to be modified.
 *
 * @return buffer - Value of the register.
*******************************************************************************/
uint32_t AD7190::GetRegisterValue(uint8_t registerAddress,
                                      uint8_t bytesNumber,
                                      uint8_t modifyCS)
{
    uint8_t registerWord[5] = {0, 0, 0, 0, 0};
    uint32_t buffer          = 0x0;
    uint8_t i               = 0;
    
    registerWord[0] = COMM_READ |
                      COMM_ADDR(registerAddress);
    Select();
    sspi->transfer(registerWord, bytesNumber + 1);
    Unselect();
    for(i = 1; i < bytesNumber + 1; i++) 
    {
        buffer = (buffer << 8) + registerWord[i];
    }

    modifyCS++; // remove not-used warning
    
    return buffer;
}


/***************************************************************************//**
 * @brief Resets the device.
 *
 * @return none.
*******************************************************************************/
void AD7190::Reset(void)
{
    uint8_t registerWord[7];
    
    // TODO: values arn't transmitted correctly (measured with oszi)


    // Test-Write, to bring all signals in order. 
    uint32_t a = 0xaaaaaaaa;
    sspi->transfer((uint8_t*)(&a), 4);

    delay(1);

    registerWord[0] = 0x01;
    registerWord[1] = 0xFF;
    registerWord[2] = 0xFF;
    registerWord[3] = 0xFF;
    registerWord[4] = 0xFF;
    registerWord[5] = 0xFF;
    registerWord[6] = 0xFF;

PRINT_LINE;
    Select();
PRINT_LINE;
    sspi->transfer(registerWord, 7);
PRINT_LINE;
    Unselect();
PRINT_LINE;
}

/***************************************************************************//**
 * @brief Set device to idle or power-down.
 *
 * @param pwrMode - Selects idle mode or power-down mode.
 *                  Example: 0 - power-down
 *                           1 - idle
 *
 * @return none.
*******************************************************************************/
void AD7190::SetPower(uint8_t pwrMode)
{
     uint32_t oldPwrMode = 0x0;
     uint32_t newPwrMode = 0x0;
 
     oldPwrMode = GetRegisterValue(REG_MODE, 3, 1);
     oldPwrMode &= ~(MODE_SEL(0x7));
     newPwrMode = oldPwrMode | 
                  MODE_SEL((pwrMode * (MODE_IDLE)) |
                                  (!pwrMode * (MODE_PWRDN)));
     SetRegisterValue(REG_MODE, newPwrMode, 3, 1);
}

/***************************************************************************//**
 * @brief Waits for RDY pin to go low.
 *
 * @return none.
*******************************************************************************/
void AD7190::WaitRdyGoLow(void)
{
    uint32_t timeOutCnt = 0xFFFFFF;
    
    while(digitalRead(pinMISO) && timeOutCnt--);
}

/***************************************************************************//**
 * @brief Selects the channel to be enabled.
 *
 * @param channel - Selects a channel.
 *  
 * @return none.
*******************************************************************************/
void AD7190::ChannelSelect(uint16_t channel)
{
    uint32_t oldRegValue = 0x0;
    uint32_t newRegValue = 0x0;
     
    oldRegValue = GetRegisterValue(REG_CONF, 3, 1);
    oldRegValue &= ~(CONF_CHAN(0xFF));
    newRegValue = oldRegValue | CONF_CHAN(1 << channel);   
    SetRegisterValue(REG_CONF, newRegValue, 3, 1);
}

/***************************************************************************//**
 * @brief Performs the given calibration to the specified channel.
 *
 * @param mode - Calibration type.
 * @param channel - Channel to be calibrated.
 *
 * @return none.
*******************************************************************************/
void AD7190::Calibrate(uint8_t mode, uint8_t channel)
{
    uint32_t oldRegValue = 0x0;
    uint32_t newRegValue = 0x0;
    
    ChannelSelect(channel);
    oldRegValue = GetRegisterValue(REG_MODE, 3, 1);
    oldRegValue &= ~MODE_SEL(0x7);
    newRegValue = oldRegValue | MODE_SEL(mode);
    Select(); 
    SetRegisterValue(REG_MODE, newRegValue, 3, 0); // CS is not modified.
    WaitRdyGoLow();
    Unselect();
}

/***************************************************************************//**
 * @brief Selects the polarity of the conversion and the ADC input range.
 *
 * @param polarity - Polarity select bit. 
                     Example: 0 - bipolar operation is selected.
                              1 - unipolar operation is selected.
* @param range - Gain select bits. These bits are written by the user to select 
                 the ADC input range.     
 *
 * @return none.
*******************************************************************************/
void AD7190::RangeSetup(uint8_t polarity, uint8_t range)
{
    uint32_t oldRegValue = 0x0;
    uint32_t newRegValue = 0x0;
    
    oldRegValue = GetRegisterValue(REG_CONF,3, 1);
    oldRegValue &= ~(CONF_UNIPOLAR |
                     CONF_GAIN(0x7));
    newRegValue = oldRegValue | 
                  (polarity * CONF_UNIPOLAR) |
                  CONF_GAIN(range); 
    SetRegisterValue(REG_CONF, newRegValue, 3, 1);
}

/***************************************************************************//**
 * @brief Returns the result of a single conversion.
 *
 * @return regData - Result of a single analog-to-digital conversion.
*******************************************************************************/
uint32_t AD7190::SingleConversion(void)
{
    uint32_t command = 0x0;
    uint32_t regData = 0x0;
 
    command = MODE_SEL(MODE_SINGLE) | 
              MODE_CLKSRC(CLK_INT) |
              MODE_RATE(0x060);    
    Select();
    SetRegisterValue(REG_MODE, command, 3, 0); // CS is not modified.
    WaitRdyGoLow();
    regData = GetRegisterValue(REG_DATA, 3, 0);
    Unselect();
    
    return regData;
}


/***************************************************************************//**
 * @brief Returns the result when continuous conversion is active.
 *      waits for data-ready signal on MISO-line. 
 *
 * @return regData - Result of a analog-to-digital conversion.
*******************************************************************************/
uint32_t AD7190::SingleRead()
{
    Select();
    WaitRdyGoLow();
    uint8_t data[3]={0};

    sspi->transfer(0x58); 
    sspi->transfer(data, 3);
    uint32_t value = ((uint32_t)(data[0])<<16) + ((uint32_t)data[1]<<8) + ((uint32_t)data[2]);
    //uint32_t value = data[2];
    
    return value;
}


/***************************************************************************//**
 * @brief Returns the average of several conversion results.
 *
 * @return samplesAverage - The average of the conversion results.
*******************************************************************************/
uint32_t AD7190::ContinuousReadAvg(uint8_t sampleNumber)
{
    uint32_t samplesAverage = 0x0;
    uint8_t count = 0x0;
    uint32_t command = 0x0;
    
    command = MODE_SEL(MODE_CONT) | 
              MODE_CLKSRC(CLK_INT) |
              MODE_RATE(0x060);
    Select();
    SetRegisterValue(REG_MODE, command, 3, 0); // CS is not modified.
    for(count = 0;count < sampleNumber;count ++)
    {
        WaitRdyGoLow();
        samplesAverage += GetRegisterValue(REG_DATA, 3, 0); // CS is not modified.
    }
    Unselect();
    samplesAverage = samplesAverage / sampleNumber;
    
    return samplesAverage ;
}

/***************************************************************************//**
 * @brief Read data from temperature sensor and converts it to Celsius degrees.
 *
 * @return temperature - Celsius degrees.
*******************************************************************************/
uint32_t AD7190::TemperatureRead(void)
{
    uint8_t temperature = 0x0;
    uint32_t dataReg = 0x0;
    
    RangeSetup(0, CONF_GAIN_1);
    ChannelSelect(CH_TEMP_SENSOR);
    dataReg = SingleConversion();
    dataReg -= 0x800000;
    dataReg /= 2815;   // Kelvin Temperature
    dataReg -= 273;    //Celsius Temperature
    temperature = (uint32_t) dataReg;
    
    return temperature;
}



void AD7190::Select()
{
    sspi->select();
}
void AD7190::Unselect()
{
    sspi->unselect();
}