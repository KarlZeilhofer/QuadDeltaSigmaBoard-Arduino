/***************************************************************************//**
 *   @file   AD7190.h
 *   @brief  Header file of AD7190 Driver.
 *   @author DNechita (Dan.Nechita@analog.com)
********************************************************************************
 * Copyright 2012(c) Analog Devices, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *  - The use of this software may or may not infringe the patent rights
 *    of one or more patent holders.  This license does not release you
 *    from the requirement that you obtain separate licenses from these
 *    patent holders to use this software.
 *  - Use of the software either in source or binary form, must be run
 *    on or directly connected to an Analog Devices Inc. component.
 *
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
********************************************************************************
 *   SVN Revision: 903
*******************************************************************************/

#ifndef __AD7190_H__
#define __AD7190_H__



/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <stdint.h>
#include <Arduino.h>
#include "software_spi.h"


extern uint8_t pinMISO;
extern uint8_t pinMOSI;
extern uint8_t pinSCK;

/******************************************************************************/
/******************************** AD7190 **************************************/
/******************************************************************************/


/* AD7190 GPIO */

#define AD7190_RDY_STATE       digitalRead(pinMISO) // check state of Port B Input 4 (MISO)

class AD7190
{

public:
	/* AD7190 Register Map */
	enum 
	{
		REG_COMM         =0, // Communications Register (WO, 8-bit) 
		REG_STAT         =0, // Status Register         (RO, 8-bit) 
		REG_MODE         =1, // Mode Register           (RW, 24-bit 
		REG_CONF         =2, // Configuration Register  (RW, 24-bit)
		REG_DATA         =3, // Data Register           (RO, 24/32-bit) 
		REG_ID           =4, // ID Register             (RO, 8-bit) 
		REG_GPOCON       =5, // GPOCON Register         (RW, 8-bit) 
		REG_OFFSET       =6, // Offset Register         (RW, 24-bit 
		REG_FULLSCALE    =7 // Full-Scale Register     (RW, 24-bit)
	};
	/* Communications Register Bit Designations (AD7190_REG_COMM) */
	enum 
	{
		COMM_WEN         =(1 << 7),           // Write Enable. 
		COMM_WRITE       =(0 << 6),           // Write Operation.
		COMM_READ        =(1 << 6),           // Read Operation. 
		COMM_CREAD       =(1 << 2)           // Continuous Read of Data Register.
	};
	inline uint32_t COMM_ADDR(uint32_t x)
		{return (((x) & 0x7) << 3);}; // Register Address. 

	/* Status Register Bit Designations (AD7190_REG_STAT) */
	enum 
	{
		STAT_RDY         =(1 << 7), // Ready.
		STAT_ERR         =(1 << 6), // ADC error bit.
		STAT_NOREF       =(1 << 5), // Error no external reference. 
		STAT_PARITY      =(1 << 4), // Parity check of the data register. 
		STAT_CH2         =(1 << 2), // Channel 2. 
		STAT_CH1         =(1 << 1), // Channel 1. 
		STAT_CH0         =(1 << 0) // Channel 0. 
	};	

	/* Mode Register Bit Designations (AD7190_REG_MODE) */
	enum 
	{
		MODE_DAT_STA     =((uint32_t)1 << 20),           // Status Register transmission.
		MODE_SINC3       =((uint32_t)1 << 15),           // SINC3 Filter Select.
		MODE_ENPAR       =((uint32_t)1 << 13),           // Parity Enable.
		MODE_SCYCLE      =((uint32_t)1 << 11),           // Single cycle conversion.
		MODE_REJ60       =((uint32_t)1 << 10)           // 50/60Hz notch filter.
	};
	inline uint32_t MODE_SEL(uint32_t x)
		{return ((((uint32_t)(x)) & 0x7) << 21);}; // Operation Mode Select.
	inline uint32_t MODE_CLKSRC(uint32_t x)
		{return ((((uint32_t)(x)) & 0x3) << 18);};  // Clock Source Select.
	inline uint32_t MODE_RATE(uint32_t x)
		{return (((uint32_t)(x)) & 0x3FF);};     // Filter Update Rate Select.

	/* Mode Register: AD7190_MODE_SEL(x) options */
	enum 
	{
		MODE_CONT                =0, // Continuous Conversion Mode.
		MODE_SINGLE              =1, // Single Conversion Mode.
		MODE_IDLE                =2, // Idle Mode.
		MODE_PWRDN               =3, // Power-Down Mode.
		MODE_CAL_INT_ZERO        =4, // Internal Zero-Scale Calibration.
		MODE_CAL_INT_FULL        =5, // Internal Full-Scale Calibration.
		MODE_CAL_SYS_ZERO        =6, // System Zero-Scale Calibration.
		MODE_CAL_SYS_FULL        =7 // System Full-Scale Calibration.
	};

	/* Mode Register: AD7190_MODE_CLKSRC(x) options */
	enum 
	{
		CLK_EXT_MCLK1_2          =0, // External crystal. The external crystal
	                                          // is connected from MCLK1 to MCLK2.
		CLK_EXT_MCLK2            =1, // External Clock applied to MCLK2 
		CLK_INT                  =2, // Internal 4.92 MHz clock. 
	                                          // Pin MCLK2 is tristated.
		CLK_INT_CO               =3 // Internal 4.92 MHz clock. The internal
	                                          // clock is available on MCLK2.
	};

	/* Configuration Register Bit Designations (AD7190_REG_CONF) */
	enum 
	{
		CONF_CHOP        =((uint32_t)1 << 23),            // CHOP enable.
		CONF_REFSEL      =((uint32_t)1 << 20),            // REFIN1/REFIN2 Reference Select.
		CONF_BURN        =((uint32_t)1 << 7),             // Burnout current enable.
		CONF_REFDET      =((uint32_t)1 << 6),             // Reference detect enable.
		CONF_BUF         =((uint32_t)1 << 4),             // Buffered Mode Enable.
		CONF_UNIPOLAR    =((uint32_t)1 << 3)             // Unipolar/Bipolar Enable.
	};
    inline static uint32_t CONF_CHAN(uint32_t x)
		{return ((((uint32_t)(x)) & 0xFF) << 8);};  // Channel select.
    inline static uint32_t CONF_GAIN(uint32_t x)
		{return (((uint32_t)(x)) & 0x7);};       // Gain Select.

	/* Configuration Register: AD7190_CONF_CHAN(x) options */
	enum 
	{
		CH_AIN1P_AIN2M      =0, // AIN1(+) - AIN2(-)       
		CH_AIN3P_AIN4M      =1, // AIN3(+) - AIN4(-)       
		CH_TEMP_SENSOR      =2, // Temperature sensor       
		CH_AIN2P_AIN2M      =3, // AIN2(+) - AIN2(-)       
		CH_AIN1P_AINCOM     =4, // AIN1(+) - AINCOM       
		CH_AIN2P_AINCOM     =5, // AIN2(+) - AINCOM       
		CH_AIN3P_AINCOM     =6, // AIN3(+) - AINCOM       
		CH_AIN4P_AINCOM     =7 // AIN4(+) - AINCOM
	};

	/* Configuration Register: AD7190_CONF_GAIN(x) options */
	//                                             ADC Input Range (5 V Reference)
	enum 
	{
		CONF_GAIN_1		=0, // Gain 1    +-5 V
		CONF_GAIN_8		=3, // Gain 8    +-625 mV
		CONF_GAIN_16	=4, // Gain 16   +-312.5 mV
		CONF_GAIN_32	=5, // Gain 32   +-156.2 mV
		CONF_GAIN_64	=6, // Gain 64   +-78.125 mV
		CONF_GAIN_128	=7 // Gain 128  +-39.06 mV
	};
	
	/* ID Register Bit Designations (AD7190_REG_ID) */
	enum 
	{
		ID_AD7190        =0x4,
		ID_MASK          =0x0F
	};

	/* GPOCON Register Bit Designations (AD7190_REG_GPOCON) */
	enum 
	{
		GPOCON_BPDSW     =(1 << 6), // Bridge power-down switch enable
		GPOCON_GP32EN    =(1 << 5), // Digital Output P3 and P2 enable
		GPOCON_GP10EN    =(1 << 4), // Digital Output P1 and P0 enable
		GPOCON_P3DAT     =(1 << 3), // P3 state
		GPOCON_P2DAT     =(1 << 2), // P2 state
		GPOCON_P1DAT     =(1 << 1), // P1 state
		GPOCON_P0DAT     =(1 << 0) // P0 state
	};

	/******************************************************************************/
	/*********************** Functions Declarations *******************************/
	/******************************************************************************/


	AD7190(uint8_t pinCS);

	/*! Checks if the AD7190 part is present. */
	uint8_t Init();

	/*! Resets the device. */
	void Reset(void);

	/*! Set device to idle or power-down. */
	void SetPower(uint8_t pwrMode);

	/*! Waits for RDY pin to go low. */
	void WaitRdyGoLow(void);

	/*! Selects the channel to be enabled. */
	void ChannelSelect(uint16_t channel);

	/*! Performs the given calibration to the specified channel. */
	void Calibrate(uint8_t mode, uint8_t channel);

	/*! Selects the polarity of the conversion and the ADC input range. */
	void RangeSetup(uint8_t polarity, uint8_t range);

	/*! Returns the result of a single conversion. */
	uint32_t SingleConversion(void);

	/*! Returns the result when continuous conversion is active. */
	uint32_t SingleRead();

	/*! Returns the average of several conversion results. */
	uint32_t ContinuousReadAvg(uint8_t sampleNumber);

	/*! Read data from temperature sensor and converts it to Celsius degrees. */
	uint32_t TemperatureRead(void);

	inline void Select();
	inline void Unselect();


	/*! Writes data into a register. */
	void SetRegisterValue(uint8_t registerAddress,
	                             uint32_t registerValue,
	                             uint8_t bytesNumber,
	                             uint8_t modifyCS);

	/*! Reads the value of a register. */
	uint32_t GetRegisterValue(uint8_t registerAddress,
	                                      uint8_t bytesNumber,
	                                      uint8_t modifyCS);

private:
	SoftwareSPI *sspi;
};


#endif /* __AD7190_H__ */
