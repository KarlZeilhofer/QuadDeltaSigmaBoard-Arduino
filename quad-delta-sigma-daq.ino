

/*

Quad Delta-Sigma Data Aquision
------------------------------

for Arduino Nano, connected to 4 ADCs (AD7190)
(c) Mechatronik Karl Zeilhofer
22.2.2016

General program description:

 - power up
 - configure each ADC with static parameters (hard coded)
    + channel: Ch3-Ch4
    + gain adjustable (see gains[])
    + bipolar
    + buffers on
    + chopping on
    + current off
    + Ref-Input 1
    + Ref-detect off
    + continious conversion
    + FS bits: Samplarate adjustable (see SAMPLERATE)
    + 60Hz rejection off
    + sinc-4 filter
    + zero latency: off
    + use external clock: 4.9152MHz
    + enable all 4 ditigtal I/Os
    + enable BPDSW
    + output: data only, no parity

 - sync all 4 ADCs with SYNC-line

 - poll ready-signal on Ch0,
 - read data from Ch0, Ch1, Ch2 and Ch3
 - send data to UART (USB)
    + format: each channel-data consists of 4 bytes.
        first byte is a maker byte in ASCII (A,B,C,D)
        second byte is the MSB of the 24-bit word.
        fourth byte is the LSB of the 24-bit word.
    + max. data-rate:
        with a baudrate of 115200 bit/s = 11520 byte/s
        we can transfer 2880 S/s,
        with 4 channels we can sample with 720 S/s... theoretically
        with the FTDI and a 16MHz-Arduino 2 Mbit/s should be possible
        which results in 50kS/s or 4x 12.5kS/s

*/


#include <stdint.h>
#include "software_spi.h"
#include "AD7190.h"

//#define ENABLE_DEBUG
#include "uartdebug.h"

//#define BINARY_OUTPUT
#define ASCII_OUTPUT

char __print_line_buf[200];

int N_ADCs = 4;


uint8_t pinSync = 6; // active low

// following pins are used by external in AD7190.cpp
uint8_t pinMISO = 12;
uint8_t pinMOSI = 11;
uint8_t pinSCK = 13;


AD7190 adc0(2);
AD7190 adc1(3);
AD7190 adc2(4);
AD7190 adc3(5);

AD7190* adcs[] = {&adc0, &adc1, &adc2, &adc3};

double Vref = 5.0; // Volts
#define SAMPLERATE  2 // samplerate in S/s, je ADC
int gains[] = {AD7190::CONF_GAIN_1, // +-5V
               AD7190::CONF_GAIN_32, // +-156.2 mV
               AD7190::CONF_GAIN_1, // +-5V
               AD7190::CONF_GAIN_32}; // +-156.2 mV

void syncADCs()
{
    digitalWrite(pinSync, LOW);
    delayMicroseconds(5); // TODO: check value
    digitalWrite(pinSync, HIGH);
}

void setup() 
{

    Serial.begin(115200);

    pinMode(pinSync, OUTPUT);
    digitalWrite(pinSync, HIGH);

    for(uint8_t ch=0; ch<N_ADCs; ch++)
    {
        uint8_t ret;
        do
        {
            ret = adcs[ch]->Init(); // reset and check connection
            if (ret == 100) { // SPI_Init() failed
                PRINT_LINE;
            } else if (ret == 0) { // wrong ID
                PRINT_LINE;
            }
        }while(ret != 1);


        uint32_t modeReg = 0x040000 + constrain((1200/SAMPLERATE)&0xFFF0, 1, 1023);
            // mask F9:0 to values multiple of 16, to ensure proper full-scale calibration. see data page 36, right column, 5th paragraph
        uint32_t confReg = 0x800210 + AD7190::CONF_GAIN(gains[ch]);
        uint8_t gpoConReg = 0x70;
        adcs[ch]->SetRegisterValue(AD7190::REG_CONF, confReg, 3, 0);
        adcs[ch]->SetRegisterValue(AD7190::REG_MODE, modeReg, 3, 0);
        adcs[ch]->SetRegisterValue(AD7190::REG_GPOCON, gpoConReg , 1, 0); // enable all outputs

        if(confReg != adcs[ch]->GetRegisterValue(AD7190::REG_CONF, 3, 0))
        {
            Serial.print("confReg cannot be validated\n");
        }
        if(modeReg != adcs[ch]->GetRegisterValue(AD7190::REG_MODE, 3, 0))
        {
            Serial.print("modeReg cannot be validated\n");
        }
        if(gpoConReg !=adcs[ch]->GetRegisterValue(AD7190::REG_GPOCON , 1, 0))
        {
            Serial.print("gpoConReg cannot be validated\n");
        }

#ifdef ENABLE_DEBUG
        Serial.print("Channel ");
        Serial.println(ch);
        Serial.println("Registers before calibration:");
        Serial.print("Offset: ");
        Serial.println(String(adcs[ch]->GetRegisterValue(AD7190::REG_OFFSET, 3, 0), HEX));
        Serial.print("Fullscale: ");
        Serial.println(String(adcs[ch]->GetRegisterValue(AD7190::REG_FULLSCALE, 3, 0), HEX));
#endif

        adcs[ch]->Calibrate(AD7190::MODE_CAL_INT_ZERO, AD7190::CH_AIN3P_AIN4M);
        adcs[ch]->Calibrate(AD7190::MODE_CAL_INT_FULL, AD7190::CH_AIN3P_AIN4M);
        adcs[ch]->SetRegisterValue(AD7190::REG_MODE, modeReg, 3, 0);

#ifdef ENABLE_DEBUG
        Serial.println("Registers after calibration:");
        Serial.print("Offset: ");
        Serial.println(String(adcs[ch]->GetRegisterValue(AD7190::REG_OFFSET, 3, 0), HEX));
        Serial.print("Fullscale: ");
        Serial.println(String(adcs[ch]->GetRegisterValue(AD7190::REG_FULLSCALE, 3, 0), HEX));
#endif

    }

    syncADCs();
    digitalWrite(pinMISO, HIGH); // turn on pullup for MISO
}

void loop() 
{
#ifdef BINARY_OUTPUT
    for(uint8_t ch=0; ch<N_ADCs; ch++)
    {
        Serial.write((uint8_t)('A' + ch)); // marker byte
        uint32_t value = adcs[ch]->SingleRead();

        Serial.write((uint8_t)((value&0xFF0000LL) >> 16));
        Serial.write((uint8_t)((value&0x00FF00LL) >> 8));
        Serial.write((uint8_t)((value&0x0000FFLL) >> 0));
    }
#endif
#ifdef ASCII_OUTPUT
    double values[4];
    for(uint8_t ch=0; ch<N_ADCs; ch++)
    {
        values[ch] = adcs[ch]->SingleRead();
    }

    Serial.print(String((double)millis()/1000, 3) + String(";"));

    for(uint8_t ch=0; ch<N_ADCs; ch++)
    {
        double value = values[ch];

        value -= 0x800000; // remove offset
        value /= 0x800000; // normalize to +-Vref
        value *= Vref;

        switch(gains[ch]){
        case AD7190::CONF_GAIN_1: // Gain 1    +-5 V @ Vref = 5V
            value /= 1; break;
        case AD7190::CONF_GAIN_8: // Gain 8    +-625 mV @ Vref = 5V
            value /= 8; break;
        case AD7190::CONF_GAIN_16: // Gain 16   +-312.5 mV @ Vref = 5V
            value /= 16; break;
        case AD7190::CONF_GAIN_32: // Gain 32   +-156.2 mV @ Vref = 5V
            value /= 32; break;
        case AD7190::CONF_GAIN_64: // Gain 64   +-78.125 mV @ Vref = 5V
            value /= 64; break;
        case AD7190::CONF_GAIN_128: // Gain 128  +-39.06 mV   @ Vref = 5V
            value *= 128; break;
        }
        // now we have value in Volts for any gain.

        Serial.print(String(value, 9) + String(";")); // with nano-volt resolution
    }
    Serial.print("\r\n");
#endif
}

void print(const char* str)
{
    Serial.print(str);
}
