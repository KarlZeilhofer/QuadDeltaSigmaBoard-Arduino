/*
 * uartdebug.h
 *
 *  Created on: 11.03.2014
 *      Author: Karl Zeilhofer
 */

//#include <avr/io.h>

// um debug-ausgaben zu entfernen, folgendes define auskommentieren:
//#define ENABLE_DEBUG

#ifndef __UART_DEBUG
#define __UART_DEBUG


#ifdef __cplusplus
extern "C" {
#endif


 #include <string.h>
extern void print(const char* str);
extern char __print_line_buf[];


#ifdef ENABLE_DEBUG
#define uartDbg(ch)  Serial.println(ch)
#else
#define uartDbg(ch)
#endif

#ifdef ENABLE_DEBUG
#define PRINT_LINE sprintf(__print_line_buf, "%s\n  %s()\n  Line %d\n", __FILE__, __func__, __LINE__); print(__print_line_buf)
#else
#define PRINT_LINE 
#endif

#ifdef __cplusplus
}
#endif


#endif