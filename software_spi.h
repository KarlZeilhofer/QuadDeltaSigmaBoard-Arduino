/*******************************************
* 
* Author: Karl Zeilhofer @ zeilhofer.co.at
* based on code from
*
* Qingfeng Xia  @ iesensor.com
* BSD 3 claused Licensed  2013
*********************************************/


#ifndef _SOFTWARE_SPI_H
#define _SOFTWARE_SPI_H

#include <Arduino.h>
#include <stdint.h>

class SoftwareSPI
{

public: 
  SoftwareSPI(uint8_t SCK, uint8_t MISO, uint8_t MOSI, uint8_t CS, 
  bool clockPolarity, bool clockPhase, bool msbFirst);

  //new-operator with specific memory address:
  // refer to http://arduino.land/FAQ/content/4/20/en/can-i-use-new-and-delete-with-arduino.html
  void *operator new( size_t size, void *ptr )
  {
    size++; // remove not-used warning
    return ptr;
  };
  void operator delete( void *obj, void *alloc )
  {
    int* o = (int*)obj; // remove not-used warning
    o++;
    int* a = (int*)alloc; // remove not-used warning
    a++;
    return;
  };

  uint8_t transfer(uint8_t b) ;
  uint8_t transfer(uint8_t* data, uint8_t count) ;
  inline void select() 
  {
    if(pinActiveCS)
    {
      digitalWrite(pinActiveCS, HIGH);  
    }
    
    digitalWrite(pinCS, LOW);
    pinActiveCS = pinCS;
  };
  inline void unselect() 
  {
    digitalWrite(pinCS, HIGH);
    pinActiveCS = 0;
  };

private:
  uint8_t bitsToByte();

  uint8_t pinMISO;
  uint8_t pinMOSI;
  uint8_t pinSCK;
  uint8_t pinCS;

  bool clockPolarity;
  bool clockPhase;
  bool msbFirst;

  unsigned char msk[8];

// static
  static uint8_t bits[8]; // input buffer
  static uint8_t pinActiveCS; // 0 if none is selected
    // is somehow a kind of a semaphore. 
};

#endif // header
